#Copyright (C) 2014 Athanasios Anastasiou
#
# Defines the main ANTLR (v4) parser in PyParsing.
# This file is also a general example of the sort of output expected from
# the a2p.py script. 
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

#Standard imports for PyParsing
from pyparsing import Optional, ZeroOrMore, OneOrMore, Empty, Regex, Literal, Forward, Group, cStyleComment, Suppress, commaSeparatedList, Combine, delimitedList, Dict, ParseException

#All 'source code', including actions that in PyParsing have a specific 
#prototype, can be collected in the beginning of the resulting file.

def subTerminal(s,loc,tok):
    '''Returns the literal or reference of the terminal'''
    return tok[0][0]

def subRuleRef(s,loc,tok):
    '''Simply returns the ruleref'''
    return tok[0][0]
    
def subAtom(s,loc,tok):
    '''Returns the atom expression'''
    if (tok.atom.terminal):
        return tok.atom.terminal
    if (tok.atom.ruleref):
        return tok.atom.ruleref
    if (tok.atom.range):
        return "Regex('[%s-%s]')" % (tok.atom.range[0][1:-1],tok.atom.range[2][1:-1])
    if (tok.atom.notSet):
        return tok.atom.notSet
    if (tok.atom.DOT):
        return "."
        
def subSetElement(s,loc,tok):
    '''Returns the setElement'''
    if (tok.setElement.TOKEN_REF):
        return tok.setElement.TOKEN_REF
        
    if (tok.setElement.STRING_LITERAL):
        return tok.setElement.STRING_LITERAL
        
    if (tok.setElement.range):
        return "[%s-%s]" % (tok.setElement.range[0][1:-1],tok.setElement.range[2][1:-1])
        
    if (tok.LEXER_CHAR_SET):
        return LEXER_CHAR_SET
        
def subLEXER_CHAR_SET(s,loc,tok):
    return "Regexp('%s')" % tok[0]
    
def subBlockSet(s,loc,tok):
    return "|".join(map(lambda x:x.setElement,tok.blockSet.setElements))
    
def subNotSet(s,loc,tok):
    return "Regex('[^%s]')" % tok.notSet[1]

def subElement(s,loc,tok):
    print tok.element.keys()
    
        
def getParser():
    '''Returns a PyParsing parser for ANTLR v4'''
    #NOTE: This is largely based on code from: 
    #https://github.com/antlr/grammars-v4/tree/master/antlr4 
    #Many thanks to David Whitten for pointing them out:
    #https://groups.google.com/forum/#!topic/antlr-discussion/JQUEK3nGPAI
    #
    ACTIONContent = Regex("[^{}]*")
    ACTIONblock = Forward()
    ACTIONblock << Combine(Literal("{") + (ZeroOrMore(ACTIONblock)&ACTIONContent) + Literal("}"))

    ARGACTIONContent = Regex("[^\\[\\]]*")
    ARGACTION = Forward()
    ARGACTION << Combine(Literal(r"[") + (ZeroOrMore(ARGACTION) & ARGACTIONContent) + Literal("]"))

    LEXER_CHAR_SET = Regex("\\[([^[\\]\\\\]|\\\\.)+\\]")
    LEXER_CHAR_SET.setParseAction(subLEXER_CHAR_SET)

    ACTION = ACTIONblock;
    ARG_ACTION = ARGACTION
    INT = Regex("[0-9]+")

    EOF = Regex("\r?\n")
    #DOC_COMMENT = Regex("/\*.*?\*/")
    DOC_COMMENT = cStyleComment
    BLOCK_COMMENT = Regex("/\\*.*?\\*/")
    #LINE_COMMENT = Regex("//[^\r\n]*")

    #ESC_SEQ = Literal('\\') + (Regex("[\\btnfr""']") ^ UNICODE_ESC ^ Regex(".") ^ EOF)   
    HEX_DIGIT = Regex("[0-9a-fA-F]");
    UNICODE_ESC = Optional(HEX_DIGIT + Optional(HEX_DIGIT + Optional(HEX_DIGIT + Optional(HEX_DIGIT))))
    ESC_SEQ = Regex("\\\\([btnfr\\\\])|.") ^ UNICODE_ESC

    OPTIONS = Regex("options[\t\f\r\n ]*{")
    TOKENS = Regex("tokens[ \t\f\n\r]*{")

    TOKEN_REF = Regex("[A-Z][a-zA-Z0-9_]*")
    RULE_REF = Regex("[a-z][a-zA-Z0-9_]*")


    IMPORT       = Literal('import')
    FRAGMENT     = Literal('fragment')             
    LEXER        = Literal('lexer')                
    PARSER       = Literal('parser')               
    GRAMMAR      = Literal('grammar')             
    PROTECTED    = Literal('protected')            
    PUBLIC       = Literal('public')              
    PRIVATE      = Literal('private')             
    RETURNS      = Literal('returns')             
    LOCALS       = Literal('locals')               
    THROWS       = Literal('throws')               
    CATCH        = Literal('catch')                
    FINALLY      = Literal('finally')              
    MODE         = Literal('mode')                 

    COLON        = Literal(':')
    COLONCOLON   = Literal('::')
    COMA         = Literal(',')
    SEMI         = Literal(';')
    LPAREN       = Literal('(')
    RPAREN       = Literal(')')
    RARROW       = Literal('->')
    LT           = Literal('<')
    GT           = Literal('>')
    ASSIGN       = Literal('=')
    QUESTION     = Literal('?')
    STAR         = Literal('*')
    PLUS         = Literal('+')
    PLUS_ASSIGN  = Literal('+=')
    OR           = Literal('|')
    DOLLAR       = Literal('$')
    DOT		     = Literal('.')
    RANGE        = Literal('..')
    AT           = Literal('@')
    POUND        = Literal('#')
    NOT          = Literal('~')
    RBRACE       = Literal('}')

    STRING_LITERAL = Regex("'([^'\\r\\n\\\\]|\\\\.)*?'")   

    optionsSpec = Forward()
    ruleAction = Forward()
    block = Forward()
    altList = Forward()
    lexerAltList = Forward()

    _id = (RULE_REF ^ TOKEN_REF)

    elementOption = Group(_id ^ \
                    (_id + ASSIGN + (_id ^ STRING_LITERAL)))

    elementOptions = LT + \
                     Group(delimitedList(elementOption, delim=COMA))("elementOption") + \
                     GT

    terminal = Group((TOKEN_REF("TOKEN_REF") + Optional(elementOptions)("elementOptions")) ^ (STRING_LITERAL("STRING_LITERAL") + Optional(elementOptions)("elementOptions")))

    _range = STRING_LITERAL + \
             RANGE + \
             STRING_LITERAL
             

    ruleref = RULE_REF + \
              Optional(ARG_ACTION)

    block << (LPAREN("LPAREN") + \
              Optional(Optional(optionsSpec)("optionsSpec") + \
              ZeroOrMore(ruleAction)("ruleActions") + COLON("COLON")) + \
              altList("altList") + \
              RPAREN("RPAREN"))

    setElement = Group(TOKEN_REF("TOKEN_REF") ^ \
                 STRING_LITERAL("STRING_LITERAL") ^ \
                 _range("range") ^ \
                 LEXER_CHAR_SET("LEXER_CHAR_SET"))("setElement")
    
    setElement.setParseAction(subSetElement)

    blockSet = Group(LPAREN("LPAREN") + \
                     delimitedList(Group(setElement("setElement")), delim = OR)("setElements") + \
                     RPAREN("RPAREN"))
                     
    blockSet.setParseAction(subBlockSet)

    notSet = Group((NOT("NOT") + setElement("setElement")) ^ \
             (NOT("NOT") + blockSet("blockSet")))
             
    notSet.setParseAction(subNotSet)

    atom = Group(_range("range") ^ \
            terminal("terminal") ^\
            ruleref("ruleref") ^ \
            notSet("notSet") ^ \
            (DOT("DOT") + Optional(elementOptions)("elementOptions")))
            
    atom.setParseAction(subAtom)

    lexerAtom = Group(_range("range") ^ \
                 terminal("terminal") ^ \
                 RULE_REF("ruleref") ^ \
                 notSet("notSet") ^ \
                 LEXER_CHAR_SET("LEXER_CHAR_SET") ^ \
                 (DOT("DOT") + Optional(elementOptions)("elementOptions")))

    ebnfSuffix = Group((QUESTION("QUESTION") + Optional(QUESTION)("QUESTION")) ^\
                 (STAR("STAR") + Optional(QUESTION)("QUESTION")) ^\
                 (PLUS("PLUS") + Optional(QUESTION)("QUESTION")))

    blockSuffix = ebnfSuffix

    ebnf = Group(block("block") + Optional(blockSuffix)("blockSuffix"))

    labeledElement = Group(_id("id") + (ASSIGN("ASSIGN")^PLUS_ASSIGN("PLUS_ASSIGN")) + (atom("atom") ^ block("block")))

    element = Group(labeledElement("labeledElement") + \
                    (ebnfSuffix("ebnfSuffix") ^ Empty()("EMPTY")) ^ \
                    (atom("atom") + (ebnfSuffix("ebnfSuffix") ^ Empty()("EMPTY"))) ^ \
                    ebnf("ebnf") ^ \
                    (ACTION("actionBody") + Optional(QUESTION)("QUESTION"))("action"))("element")
    
    element.setParseAction(subElement)

    elements = OneOrMore(Group(element))

    alternative = Group(elements("elements") ^ Empty()("EMPTY"))

    altList << delimitedList(alternative, delim=OR)

    lexerCommandExpr = _id ^ INT

    lexerCommandName = _id ^ MODE

    lexerCommand = (lexerCommandName + LPAREN + lexerCommandExpr + RPAREN) ^ \
                    lexerCommandName

    lexerCommands = RARROW + delimitedList(lexerCommand, delim=COMA)

    lexerBlock = Group(LPAREN("LPAREN") + Group(lexerAltList)("lexerAltList") + RPAREN("RPAREN"))

    labeledLexerElement = Group(_id("id") + (ASSIGN("ASSIGN") ^ PLUS_ASSIGN("PLUS_ASSIGN")) + ( lexerAtom("lexerAtom") ^ block("block") ))

    lexerElement = Group((labeledLexerElement("labeledLexerElement") + Optional(ebnfSuffix)("ebnfSuffix")) ^ \
                   (lexerAtom("lexerAtom") + Optional(ebnfSuffix)("ebnfSuffix")) ^ \
                   (lexerBlock("lexerBlock") + Optional(ebnfSuffix)("ebnfSuffix")) ^ \
                   (ACTION("actionBody") + Optional(QUESTION)("QUESTION")))("lexerElement")

    lexerElements = OneOrMore(Group(lexerElement))

    lexerAlt = Group(Optional(lexerElements)("lexerElements") + Optional(lexerCommands)("lexerCommands"))("lexerAlt")

    lexerAltList << delimitedList(Group(lexerAlt),delim=OR)

    lexerRuleBlock = Group(lexerAltList("lexerAltList"))

    lexerRule = Group(Optional(DOC_COMMENT)("DOC_COMMENT") + \
                 Optional(FRAGMENT)("FRAGMENT") + \
                 TOKEN_REF("TOKEN_REF") + \
                 COLON("COLON") + \
                 lexerRuleBlock("lexerRuleBlock") + \
                 SEMI("SEMI"))

    labeledAlt = Group(alternative("alternative") + Optional(POUND("POUND") + _id("id"))("label"))("labeledAlt")

    ruleAltList = delimitedList(Group(labeledAlt), delim=OR)

    ruleBlock = Group(ruleAltList("ruleAltList"))

    ruleModifier = (PUBLIC ^ \
                   PRIVATE ^ \
                   PROTECTED ^ \
                   FRAGMENT)

    ruleModifiers = OneOrMore(Group(ruleModifier))

    ruleAction << Group(AT("AT") + _id("ruleID") + ACTION("actionBody"))

    localsSpec = LOCALS("LOCALS") + ARG_ACTION("actionArg")

    throwsSpec = THROWS("THROWS") + delimitedList(_id, delim=COMA)("exceptionList")

    ruleReturns = RETURNS("RETURNS") + ARG_ACTION("actionArg")

    rulePrequel = (optionsSpec("optionsSpec") ^ ruleAction("ruleAction"))

    finallyClause = Group(FINALLY("FINALLY") + ACTION("actionBody"))

    exceptionHandler = Group(CATCH("CATCH") + ARG_ACTION("actionArg") + ACTION("actionBody"))

    exceptionGroup = ZeroOrMore(Group(exceptionHandler)) + Optional(finallyClause)

    parserRuleSpec = Group(Optional(DOC_COMMENT)("DOC_COMMENT") + \
                     Optional(ruleModifiers)("ruleModifiers") + \
                     RULE_REF("RULE_REF") + \
                     Optional(ARG_ACTION)("ARG_ACTION") + \
                     Optional(ruleReturns)("ruleReturns") + \
                     Optional(throwsSpec)("throwsSpec") + \
                     Optional(localsSpec)("localsSpec") + \
                     ZeroOrMore(Group(rulePrequel))("rulePrequels") +\
                     COLON("COLON") + \
                     ruleBlock("ruleBlock") + \
                     SEMI("SEMI") + \
                     exceptionGroup("exceptionGroup"))
                     
    ruleSpec = Group(parserRuleSpec("parserRuleSpec") ^ lexerRule("lexerRule"))("ruleSpec")

    rules = ZeroOrMore(Group(ruleSpec))

    modeSpec = Group(MODE("MODE") + \
               _id("id") + \
               SEMI("SEMI") + \
               OneOrMore(Group(ruleSpec))("ruleSpecList"))

    actionScopeName = _id ^ \
                      LEXER ^ \
                      PARSER;

    action = Group(AT("AT") + \
             Optional(actionScopeName + COLONCOLON)("actionScopeName") + \
             _id("id") + \
             ACTION("actionBody"))

    tokensSpec = Group(TOKENS("TOKENS") + \
                 delimitedList(_id)("tokenList") + \
                 Optional(COMA)("COMA") + \
                 RBRACE("RBRACE"));

    delegateGrammar = Group((_id("id_l") + ASSIGN("ASSIGN") + _id("id_r")) ^ \
                       _id("id"))("delegateGrammar");

    delegateGrammars = Group(IMPORT("IMPORT") + \
                       delimitedList(delegateGrammar)("delegateGrammarList") + \
                       SEMI("SEMI"))

    optionValue = Group(delimitedList(_id, delim=DOT)("optionList") ^ \
                  STRING_LITERAL("STRING_LITERAL") ^ \
                  ACTION("actionBody") ^ \
                  INT("INT"));

    option = Group(_id("id") + ASSIGN("ASSIGN") + optionValue("optionValue") + SEMI("SEMI"))("option");

    optionsSpec << Group(OPTIONS("OPTIONS") + ZeroOrMore(Group(option))("options") + RBRACE("RBRACE"));

    prequelConstruct = Group(optionsSpec("optionsSpec") ^ \
                        delegateGrammars("delegateGrammars") ^ \
                        tokensSpec("tokensSpec") ^ \
                        action("action"));

    grammarType = Group((LEXER("LEXER") + GRAMMAR("GRAMMAR")) ^ \
                  (PARSER("PARSER") + GRAMMAR("GRAMMAR")) ^ 
                  GRAMMAR("GRAMMAR"));

    grammarSpec = Group(Optional(DOC_COMMENT)("DOC_COMMENT") + \
                    grammarType("grammarType") + \
                    _id("id") + \
                    SEMI("SEMI") + \
                    ZeroOrMore(Group(prequelConstruct("prequelConstruct")))("prequelConstructs") + \
                    rules("rules") + \
                    ZeroOrMore(Group(modeSpec))("modeSpecs"))("grammarSpec")                      
                  
    #Obviously this function would return the grammar's starting rule.
    return grammarSpec
    
if __name__=="__main__":
    '''Runs a simple test of the parser on an external file'''
    #Get the parser
    p = getParser();
    #Parse the ANTLR file.
    Q = p.parseFile("./simplifiedTest_2.g4") 
    #A simple sanity check to verify the number of rules being specified in a file.
    print "Rules defined:%d" % len(Q.grammarSpec.rules)
