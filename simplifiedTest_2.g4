/* Some comment */
grammar simplifiedTest;
ALPHA:'$';
BETA:[A-Z];
GAMMA:[A-Z]*;
DELTA:'A'..'Z';
EPSILON:~'A';
ZETA:~'A'|'B';
ETA:~('A'|'B');
DELTA:('A'..'Z' 'alpha' | BETA);
EPSILON:[0-9];
ZETA:[0-9]+;
ETA:~[0-9];
THETA:~'4';
IOTA:.;
KAPPA:.*;
LAMBDA:.*?;
MU:qu=[0-9];
NU:('A'|'B');
