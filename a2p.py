#Copyright (C) 2014 Athanasios Anastasiou
#
# This will be the entry point for the code applying the transformation
# between representations.
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.

import ANTLRv4Grammar
import sys

   
if __name__ == "__main__":
    #a2p creates a .py file with the pyparsing expression of the grammar
    #described in an antlr file.
    p = ANTLRv4Grammar.getParser()
    if len(sys.argv)<2:
        sys.stdout.write("File parameter required\n")
    else:
        Q = p.parseFile(sys.argv[1])
        #Write the header
        fd = open(Q.grammarSpec.id+".py","w")
        fd.write("from pyparsing import *\n\n")
        for aRule in Q.grammarSpec.rules:
            fd.write("%s = Forward()\n" % (aRule.ruleSpec.lexerRule.TOKEN_REF if aRule.ruleSpec.lexerRule!="" else aRule.ruleSpec.parserRuleSpec.RULE_REF))
        fd.close()
